package jp.alhinc.hayashi_yoshiki.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {
	public static void main(String[] args) {
		if (args.length != 1) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}
		Map<String, String> branchNames = new HashMap<>();
		Map<String, Long> branchSales = new HashMap<>();
		if(!inputFile(args[0], branchNames, branchSales, "branch.lst", "支店", "^[0-9]{3}")) {
			return;
		}

		File[] files = new File(args[0]).listFiles();
		List<File> rcdFiles = new ArrayList<>();
		for(int i=0; i < files.length; i++) {
			if(files[i].isFile() && files[i].getName().matches("^[0-9]{8}. *rcd$")) {
				rcdFiles.add(files[i]);
			}
		}
		Collections.sort(rcdFiles);
		for(int i = 0; i < rcdFiles.size() - 1; i++) {
			int former = Integer.parseInt(rcdFiles.get(i).getName().substring(0,8));
			int latter = Integer.parseInt(rcdFiles.get(i + 1).getName().substring(0,8));
			if((latter - former) != 1) {
				System.out.println("売上ファイル名が連番になっていません");
				return;
			}
		}
		for(int i = 0; i < rcdFiles.size(); i++) {
			BufferedReader br = null;
			try {
				List<String> lineList = new ArrayList<String>();
				FileReader fr = new FileReader(rcdFiles.get(i));
				br = new BufferedReader(fr);
				String line;
				while((line = br.readLine()) != null) {
					lineList.add(line);
				}
				if(!lineList.get(1).matches("^[0-9]+$" )) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
				long fileSale = Long.parseLong(lineList.get(1));
				if(lineList.size() != 2) {
					System.out.println(lineList + "のフォーマットが不正です");
					return;
				}
				if (!branchNames.containsKey(lineList.get(0))) {
            		System.out.println(branchNames + "の支店コードが不正です");
            		 return;
            	}
				Long saleAmount = (branchSales.get(lineList.get(0)) + fileSale);
				if(saleAmount >= 10000000000L){
					System.out.println("合計金額が10桁を超えました");
					return;
				}
				branchSales.put(lineList.get(0), saleAmount);

			}catch(IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return;
			}finally {
				if(br != null) {
					try {
						br.close();
					} catch(IOException e) {
						System.out.println("予期せぬエラーが発生しました");
						return;
					}
				}
			}
		}

		if(!outputFile(args[0], branchNames, branchSales, "branch.out")){
			return;
		}

	}

	public static boolean inputFile(String path, Map<String, String> name, Map<String, Long>  sales, String fileName, String files, String text) {

		BufferedReader br = null;
		try {
			File file = new File(path, fileName);
			if(!file.exists()) {
				System.out.println(files + "定義ファイルが存在しません");
				return false;
			}
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);
			String line;
			while((line = br.readLine()) != null) {
				String[] items = line.split(",");
				if((items.length != 2) || (!items[0].matches(text))){
					System.out.println(files + "定義ファイルのフォーマットが不正です");
					return false;
				}
				name.put(items[0], items[1]);
				sales.put(items[0], 0L);
			}
		} catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		}finally {
			if(br != null) {
				try {
					br.close();
				} catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}



	public static boolean outputFile(String path, Map<String, String> name, Map<String, Long>  sales, String fileName) {

		BufferedWriter bw = null;
        try{
        	File file = new File(path, fileName);
        	FileWriter fw = new FileWriter(file);
        	bw = new BufferedWriter(fw);

        	for(String key : sales.keySet()){
        		bw.write(key + ","+ name.get(key) + "," + sales.get(key));
        		bw.newLine();
        	}
        }catch(IOException e){
            System.out.println("予期せぬエラーが発生しました");
            return false;
        }finally {
        	try {
				bw.close();
			} catch(Exception fe) {
				System.out.println("予期せぬエラーが発生しました");
				return false;
			}
		}
        return true;

	}




}
